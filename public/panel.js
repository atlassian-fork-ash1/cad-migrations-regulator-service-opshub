require(['opshub/services', 'opshub/util'], function (services, util) {
    
        const customerAccountDirectoryService = (service) => ({

            cancelMigrate: ({migrationId, reason}) => $.ajax({
                url: service.proxyUrl(`/api/migration/failed?migration-id=${migrationId}&reason=${reason}`),
                type: 'POST',
                headers: {
                    'Content-Type':'application/json'
                }
            }),

            changePoolSize: ({poolSize}) => $.ajax({
                url: service.proxyUrl(`/api/admin/pool-size?size=${poolSize}`),
                type: 'POST',
                headers: {
                    'Content-Type':'application/json'
                }
            }),

            getPoolSize: () => $.ajax({
                url: service.proxyUrl('/api/admin/pool-size'),
                type: 'GET',
                headers: {
                    'Content-Type':'application/json'
                }
            }),

            cleanup: ({timeout}) => $.ajax({
                url: service.proxyUrl(`/api/admin/cleanup?timeout-secs=${timeout}`),
                type: 'POST',
                headers: {
                    'Content-Type':'application/json'
                }
            }),

            requestMigration: ({clientId, force}) => $.ajax({
                url: service.proxyUrl(`/api/migration/request?client-id=${clientId}&force=${force}`),
                type: 'POST',
                headers: {
                    'Content-Type':'application/json'
                }
            }),

            getRecords: ({status, page, size, daysAgo}) => $.ajax({
                url: service.proxyUrl(`/api/stats/records?status=${status}&page=${page}&size=${size}&days-ago=${daysAgo}`),
                type: 'GET',
                headers: {
                    'Content-Type':'application/json'
                }
            })
        });
    
        const panelPresenter = (() => {
            const render = (html, $results, $loading) => {
                $loading.hide();
                $results.show().html(html);
            };
    
            return {
                onCancelMigrateClick: () => {
                    const service = services.selectedService();
                    const migrationId = $.trim($('#migration-id-input').val());
                    const reason = $.trim($('#reason-input').val());
    
                    const $el = $('#customer-account-directory-service-panel');
                    const $loading = $el.find('#cancel-migration-loading');
                    const $results = $el.find('#cancel-migration-results');
                    $loading.show().spin();
                    $results.hide();

                    const cancel = customerAccountDirectoryService(service).cancelMigrate({migrationId, reason});
    
                    cancel.done((data) => {
                        render(
                            `<table class="aui">
                                <thead>
                                    <tr>
                                        <th>Success?</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="field-value">${data.success}</td>
                                    <td class="field-value">${data.reason}</td>
                                </tr>
                            </table>`,
                            $results,
                            $loading
                        );
                    })
                    .fail(error => {
                        AJS.flag({
                            type: 'error',
                            title: 'Error cancelling migration',
                            body: error.responseText
                        });
                        $loading.hide();
                        $results.hide();
                    });
                },

                getPoolSize: () => {
                    const service = services.selectedService();
                    const $el = $('#customer-account-directory-service-panel');
                    const $loading = $el.find('#get-pool-size-loading');
                    const $results = $el.find('#get-pool-size-results');
                    $loading.show();
                    $results.hide();

                    const getPool = customerAccountDirectoryService(service).getPoolSize();

                    getPool.done((data) => {
                        render(
                            data,
                            $results,
                            $loading
                        );
                    })
                    .fail(error => {
                        AJS.flag({
                            type: 'error',
                            title: 'Error getting pool size',
                            body: error.responseText
                        });
                        // $loading.hide();
                        $results.hide();
                    });
                },

                changePoolSize: () => {
                    const service = services.selectedService();
                    const poolSize = $.trim($('#pool-size-input').val());

                    const $el = $('#customer-account-directory-service-panel');
                    const $loading = $el.find('#pool-size-loading');
                    const $results = $el.find('#pool-size-results');
                    $loading.show().spin();
                    $results.hide();

                    const changePool = customerAccountDirectoryService(service).changePoolSize({poolSize});

                    changePool.done((data) => {
                        render(
                            `<table class="aui">
                                    <thead>
                                        <tr>
                                            <th>Success?</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td class="field-value">${data}</td>
                                    </tr>
                                </table>`,
                            $results,
                            $loading
                        );
                        panelPresenter.getPoolSize();
                    })
                    .fail(error => {
                        AJS.flag({
                            type: 'error',
                            title: 'Error changing pool size',
                            body: error.responseText
                        });
                        $loading.hide();
                        $results.hide();
                    });
                },

                cleanup: () => {
                    const service = services.selectedService();
                    const timeout = $.trim($('#timeout-secs-input').val());

                    const $el = $('#customer-account-directory-service-panel');
                    const $loading = $el.find('#cleanup-loading');
                    const $results = $el.find('#cleanup-results');
                    $loading.show().spin();
                    $results.hide();

                    const cleanup = customerAccountDirectoryService(service).cleanup({timeout});

                    cleanup.done((data) => {
                        render(
                            `<table class="aui">
                                    <thead>
                                        <tr>
                                            <th>Number cleaned</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td class="field-value">${data}</td>
                                    </tr>
                                </table>`,
                            $results,
                            $loading
                        );
                    })
                    .fail(error => {
                        AJS.flag({
                            type: 'error',
                            title: 'Error cleaning up',
                            body: error.responseText
                        });
                        $loading.hide();
                        $results.hide();
                    });
                },

                requestMigration: () => {
                    const service = services.selectedService();
                    const clientId = $.trim($('#client-id-input').val());
                    const force = $.trim($('#force-input').val());

                    const $el = $('#customer-account-directory-service-panel');
                    const $loading = $el.find('#request-migration-loading');
                    const $results = $el.find('#request-migration-results');
                    $loading.show().spin();
                    $results.hide();

                    const request = customerAccountDirectoryService(service).requestMigration({clientId, force});

                    request.done((data) => {
                        render(
                            `<table class="aui">
                                <thead>
                                    <tr>
                                        <th>Success</th>
                                        <th>Migration ID</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="field-value">${data.success}</td>
                                    <td class="field-value">${data.migrationId}</td>
                                    <td class="field-value">${data.reason}</td>
                                </tr>
                            </table>`,
                            $results,
                            $loading
                        );
                    })
                    .fail(error => {
                        AJS.flag({
                            type: 'error',
                            title: 'Error requesting migration',
                            body: error.responseText
                        });
                        $loading.hide();
                        $results.hide();
                    });
                },

                getRecords: () => {
                    const service = services.selectedService();
                    const status = $.trim($('#records-status-input').val());
                    const page = $.trim($('#records-page-input').val());
                    const size = $.trim($('#records-size-input').val());
                    const daysAgo = $.trim($('#records-days-ago-input').val());

                    const $el = $('#customer-account-directory-service-panel');
                    const $loading = $el.find('#get-requests-loading');
                    const $results = $el.find('#get-requests-results');
                    $loading.show().spin();
                    $results.hide();

                    const records = customerAccountDirectoryService(service).getRecords({status, page, size, daysAgo});
                    let recordData = [];
                    let options = {day: "numeric", month: "numeric", year: "numeric", hour: "numeric", hour12: false, minute: "numeric", second: "numeric"};

                    records.done((data) => {
                        data.forEach(record => {
                            recordData.push(
                                `<tr>
                                    <td class="field-value">${record.id}</td>
                                    <td class="field-value">${record.clientId}</td>
                                    <td class="field-value">${record.started ? new Date(record.started).toLocaleDateString("en-US", options) : null}</td>
                                    <td class="field-value">${record.finished ? new Date(record.finished).toLocaleDateString("en-US", options) : null}</td>
                                    <td class="field-value">${record.heartbeat ? new Date(record.heartbeat).toLocaleDateString("en-US", options) : null}</td>
                                    <td class="field-value">${record.status}</td>
                                    <td class="field-value">${record.filesLeft}</td>
                                    <td class="field-value">${record.failedReason}</td>
                                </tr>`
                            )
                        });
                        render(
                            `<table class="aui">
                                <thead>
                                    <tr>
                                        <th>Migration ID</th>
                                        <th>Client ID</th>
                                        <th>Started</th>
                                        <th>Finished</th>
                                        <th>Heartbeat</th>
                                        <th>Status</th>
                                        <th>Files Left</th>
                                        <th>Failed Reason</th>
                                    </tr>
                                </thead>
                                ${recordData.join("")}
                            </table>`,
                            $results,
                            $loading
                        );
                    })
                    .fail(error => {
                        AJS.flag({
                            type: 'error',
                            title: 'Error getting records',
                            body: error.responseText
                        });
                        $loading.hide();
                        $results.hide();
                    });
                }
            }
        })();
    
        $('#cancel-migration').click(panelPresenter.onCancelMigrateClick);
        $('#change-pool-size').click(panelPresenter.changePoolSize);
        $('#cleanup').click(panelPresenter.cleanup);
        $('#get-records').click(panelPresenter.getRecords);
        $('#request-migration').click(panelPresenter.requestMigration);
        panelPresenter.getPoolSize();
        $('.loading').hide();
    
        window.panelPresenter = panelPresenter;
    });